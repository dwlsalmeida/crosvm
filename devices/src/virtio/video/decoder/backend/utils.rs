// Copyright 2021 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
#![allow(dead_code)]
use super::DecoderEvent;
use base::EventFd;
use std::collections::VecDeque;

// Manages a pollable queue of events to be sent to the decoder.
pub struct DecoderEventQueue {
    // Pipe used to signal available events.
    eventfd: EventFd,
    // FIFO of all pending events.
    pending_events: VecDeque<DecoderEvent>,
}

impl DecoderEventQueue {
    // Create a new event queue.
    pub fn new() -> sys_util::Result<Self> {
        Ok(Self {
            eventfd: EventFd::new_semaphore()?,
            pending_events: Default::default(),
        })
    }

    // Add `event` to the queue.
    pub fn queue_event(&mut self, event: DecoderEvent) -> sys_util::Result<()> {
        self.eventfd.write(1)?;
        self.pending_events.push_back(event);
        Ok(())
    }

    // Read the next event, blocking until an event becomes available.
    pub fn dequeue_event(&mut self) -> sys_util::Result<DecoderEvent> {
        self.eventfd.read()?;
        // `unwrap` is safe here because `eventfd.read` cannot return unless `queue_event` has been
        // symetrically called.
        Ok(self.pending_events.pop_front().unwrap())
    }

    // Return a `RawDescriptor` on which the caller can poll to know when `dequeue_event` can be
    // called without blocking.
    pub fn event_pipe(&self) -> &EventFd {
        &self.eventfd
    }

    // Returns the number of events currently pending on this queue, i.e. the number of times
    // `dequeue_event` can be called without blocking.
    #[cfg(test)]
    pub fn len(&self) -> usize {
        self.pending_events.len()
    }
}

#[cfg(test)]
mod tests {
    use std::time::Duration;

    use super::*;
    use crate::virtio::video::format::Rect;
    use sys_util::*;

    // Test basic queue/dequeue functionality of `DecoderEventQueue`.
    #[test]
    fn decoder_event_queue() {
        let mut event_queue = DecoderEventQueue::new().unwrap();

        assert_eq!(
            event_queue.queue_event(DecoderEvent::NotifyEndOfBitstreamBuffer(1)),
            Ok(())
        );
        assert_eq!(event_queue.len(), 1);
        assert_eq!(
            event_queue.queue_event(DecoderEvent::PictureReady {
                picture_buffer_id: 0,
                bitstream_id: 1,
                visible_rect: Rect {
                    left: 0,
                    top: 0,
                    right: 320,
                    bottom: 240,
                },
            }),
            Ok(())
        );
        assert_eq!(event_queue.len(), 2);

        assert!(matches!(
            event_queue.dequeue_event(),
            Ok(DecoderEvent::NotifyEndOfBitstreamBuffer(1))
        ));
        assert_eq!(event_queue.len(), 1);
        assert!(matches!(
            event_queue.dequeue_event(),
            Ok(DecoderEvent::PictureReady {
                picture_buffer_id: 0,
                bitstream_id: 1,
                visible_rect: Rect {
                    left: 0,
                    top: 0,
                    right: 320,
                    bottom: 240,
                }
            })
        ));
        assert_eq!(event_queue.len(), 0);
    }

    // Return the number of events that would immediately signal on `PollContext`.
    fn num_pending_events<T: PollToken>(poll_context: &PollContext<T>) -> usize {
        poll_context
            .wait_timeout(Duration::from_secs(0))
            .unwrap()
            .into_iter()
            .count()
    }

    // Test polling of `DecoderEventQueue`'s `event_pipe`.
    #[test]
    fn decoder_event_queue_polling() {
        let mut event_queue = DecoderEventQueue::new().unwrap();
        let event_pipe = event_queue.event_pipe();
        let poll_context = PollContext::new().unwrap();
        poll_context
            .add_fd_with_events(event_pipe, WatchingEvents::empty().set_read(), 0xf00u32)
            .unwrap();

        // The queue is empty, so `event_pipe` should not signal.
        assert_eq!(num_pending_events(&poll_context), 0);

        // `event_pipe` should signal as long as the queue is not empty.
        event_queue
            .queue_event(DecoderEvent::NotifyEndOfBitstreamBuffer(1))
            .unwrap();
        assert_eq!(num_pending_events(&poll_context), 1);
        event_queue
            .queue_event(DecoderEvent::NotifyEndOfBitstreamBuffer(2))
            .unwrap();
        assert_eq!(num_pending_events(&poll_context), 1);
        event_queue
            .queue_event(DecoderEvent::NotifyEndOfBitstreamBuffer(3))
            .unwrap();
        assert_eq!(num_pending_events(&poll_context), 1);
        event_queue.dequeue_event().unwrap();
        assert_eq!(num_pending_events(&poll_context), 1);
        event_queue.dequeue_event().unwrap();
        assert_eq!(num_pending_events(&poll_context), 1);
        event_queue.dequeue_event().unwrap();
        // The queue is empty again, so `event_pipe` should not signal.
        assert_eq!(num_pending_events(&poll_context), 0);
    }
}
