// Copyright 2021 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

use base::{info, warn, MappedRegion};
use ffmpeg_sys as ffi;
use std::{
    collections::{BTreeMap, VecDeque},
    ffi::CStr,
    marker::PhantomData,
};
use thiserror::Error as ThisError;

use sys_util::MemoryMapping;

use crate::virtio::video::{
    decoder::backend::utils::DecoderEventQueue,
    format::{FormatDesc, FormatRange, FrameFormat, Level, Profile},
    resource::{GuestResource, GuestResourceHandle},
};

use super::*;

// Errors we need to match again. We cannot against const fns calls, so we need to define a constant
// for these.
const AVERROR_EAGAIN: i32 = ffi::AVERROR(ffi::EAGAIN);

// TODO replace this with dedicated errors per-method?
#[derive(Debug, ThisError)]
enum AvError {
    #[error("failed to allocate AVContext object")]
    ContextAllocation,
    #[error("failed to open AVContext object")]
    ContextOpen,
    #[error("failed to open AVFrame object")]
    FrameAllocate,
    #[error("broken event pipe: {0}")]
    BrokenEventPipe(sys_util::Error),
    #[error("avcodec_send_packet returned error {0}")]
    SendPacketError(i32),
    #[error("avcodec_receive_frame returned error {0}")]
    ReceiveFrameError(i32),
    #[error("cannot perform operation because a flush is in progress")]
    FlushInProgress,
    #[error("cannot import buffer: {0}")]
    CannotImportBuffer(#[from] OutputBufferImportError),
    #[error("cannot reuse buffer: {0}")]
    CannotReuseBuffer(#[from] OutputBufferReuseError),
    #[error("operation cannot be performed in this state")]
    InvalidState,
    #[error("cannot create SWS format converter: {0}")]
    FormatConverterCreationError(#[from] AvFormatConverterCreationError),
    #[error("error while converting frame: {0}")]
    FormatConversionError(#[from] AvFormatConvertError),
}

impl From<AvError> for VideoError {
    fn from(error: AvError) -> Self {
        VideoError::BackendFailure(Box::new(error))
    }
}

struct AvCodecIterator {
    iter: *mut libc::c_void,
}

impl AvCodecIterator {
    fn new() -> Self {
        Self {
            iter: std::ptr::null_mut(),
        }
    }
}

impl Iterator for AvCodecIterator {
    // `AVCodec` instances are static within ffmpeg so their lifetime doesn't need to be regulated.
    type Item = &'static ffi::AVCodec;

    fn next(&mut self) -> Option<Self::Item> {
        // Safe because `iter` was initialized to `NULL` and we only use it with `av_codec_iterate`.
        unsafe { ffi::av_codec_iterate(&mut self.iter as *mut *mut libc::c_void).as_ref() }
    }
}

struct AvProfileIterator<'a> {
    ptr: *const ffi::AVProfile,
    phantom: PhantomData<&'a ffi::AVCodec>,
}

impl<'a> AvProfileIterator<'a> {
    /// Construct an iterator over the profiles of an `AVCodec` instance.
    fn new(codec: &'a ffi::AVCodec) -> Self {
        Self {
            ptr: codec.profiles,
            phantom: PhantomData,
        }
    }
}

impl<'a> Iterator for AvProfileIterator<'a> {
    type Item = &'a ffi::AVProfile;

    fn next(&mut self) -> Option<Self::Item> {
        // Safe because the contract of `new` stipulates we have received a valid `AVCodec`
        // reference, thus the `profiles` pointer must either be NULL or point to a valid array
        // or `VAProfile`s.
        match unsafe { self.ptr.as_ref() } {
            None => None,
            Some(profile) => {
                match profile.profile {
                    ffi::FF_PROFILE_UNKNOWN => None,
                    _ => {
                        // Safe because the contract of `new` stipulates we have received a valid
                        // `AVCodec` reference and AVCodec's `profile` array is supposed to be
                        // terminated by `FF_PROFILE_UNKNOWN`.
                        self.ptr = unsafe { self.ptr.offset(1) };
                        Some(profile)
                    }
                }
            }
        }
    }
}

/// All the parameters needed to queue an input packet passed to the codec.
struct InputPacket {
    bitstream_id: i32,
    resource: GuestResourceHandle,
    offset: u32,
    bytes_used: u32,
}
enum CodecJob {
    Packet(InputPacket),
    Flush,
}

pub struct OutputBuffer {
    picture_buffer_id: i32,
    resource: GuestResource,
}

pub struct OutputQueue {
    // Max number of output buffers that can be imported into this queue.
    num_buffers: usize,
    // Maps picture IDs to the corresponding output buffers descriptors.
    buffers: BTreeMap<i32, OutputBuffer>,
    // Picture IDs of output buffers we can write into.
    ready_buffers: VecDeque<i32>,
}

#[derive(Debug, ThisError)]
pub enum OutputBufferImportError {
    #[error("maximum number of imported buffers ({0}) already reached")]
    MaxBuffersReached(usize),
    #[error("a buffer with picture ID {0} is already imported")]
    AlreadyImported(i32),
}

#[derive(Debug, ThisError)]
pub enum OutputBufferReuseError {
    #[error("no buffer with picture ID {0} is imported at the moment")]
    NotYetImported(i32),
    #[error("buffer with picture ID {0} is already queued for use")]
    AlreadyQueued(i32),
}

impl OutputQueue {
    pub fn new(num_buffers: usize) -> Self {
        Self {
            num_buffers,
            buffers: Default::default(),
            ready_buffers: Default::default(),
        }
    }

    /// Import a buffer and make it ready to be used for decoding.
    pub fn import_buffer(&mut self, buffer: OutputBuffer) -> Result<(), OutputBufferImportError> {
        if self.buffers.len() >= self.num_buffers {
            return Err(OutputBufferImportError::MaxBuffersReached(self.num_buffers));
        }

        let picture_buffer_id = buffer.picture_buffer_id;

        if self.buffers.insert(picture_buffer_id, buffer).is_some() {
            return Err(OutputBufferImportError::AlreadyImported(picture_buffer_id));
        }

        self.ready_buffers.push_back(picture_buffer_id);

        Ok(())
    }

    pub fn reuse_buffer(&mut self, picture_buffer_id: i32) -> Result<(), OutputBufferReuseError> {
        if !self.buffers.contains_key(&picture_buffer_id) {
            return Err(OutputBufferReuseError::NotYetImported(picture_buffer_id));
        }

        if self.ready_buffers.contains(&picture_buffer_id) {
            return Err(OutputBufferReuseError::AlreadyQueued(picture_buffer_id));
        }

        self.ready_buffers.push_back(picture_buffer_id);

        Ok(())
    }

    /// Get a buffer ready to be decoded into, if any is available.
    ///
    /// This method does not remove the buffer from the ready queue: if there is a buffer ready,
    /// a promise to provide this buffer is returned. The buffer is only removed when the promise
    /// is transformed into an actual buffer reference.
    pub fn try_get_ready_buffer(&mut self) -> Option<&mut OutputBuffer> {
        let buffer_id = self.ready_buffers.pop_front()?;
        // Unwrap is safe here because our interface guarantees that ids in `ready_buffers` are
        // valid keys for `buffers`.
        Some(self.buffers.get_mut(&buffer_id).unwrap())
    }
}

/// Copies a decoded `AVFrame` into an `OutputBuffer`'s memory, while converting the pixel format if
/// needed.
struct AvFormatConverter {
    sws_context: *mut ffi::SwsContext,
    dst_format: ffi::AVPixelFormat,
}

#[derive(Debug, ThisError)]
enum AvFormatConverterCreationError {
    #[error("Unsupported format: {0}")]
    UnsupportedFormat(Format),
    #[error("error while creating the SWS context")]
    ContextCreationError,
}

#[derive(Debug, ThisError)]
enum AvFormatConvertError {
    #[error("destination buffer is too small ({0} bytes when we need at least {1}")]
    DestinationBufferTooSmall(usize, usize),
    #[error("cannot map destination buffer: {0}")]
    CannotMapBuffer(sys_util::MmapError),
    #[error("resource has no plane description, at least 1 is required")]
    NoPlanesInResource,
    #[error("resource has more planes ({0}) than ffmpeg can handle ({1})")]
    TooManyPlanesInResource(usize, usize),
}

impl Drop for AvFormatConverter {
    fn drop(&mut self) {
        // Safe because `sws_context` is valid through the life of this object.
        unsafe { ffi::sws_freeContext(self.sws_context) };
    }
}

impl AvFormatConverter {
    fn new(
        width: usize,
        height: usize,
        src_format: ffi::AVPixelFormat,
        dst_format: Format,
    ) -> Result<Self, AvFormatConverterCreationError> {
        let dst_format = match dst_format {
            Format::NV12 => ffi::AVPixelFormat::AV_PIX_FMT_NV12,
            _ => {
                return Err(AvFormatConverterCreationError::UnsupportedFormat(
                    dst_format,
                ))
            }
        };

        // Safe because we don't pass any non-null pointer to this function.
        let sws_context = unsafe {
            ffi::sws_getContext(
                width as i32,
                height as i32,
                src_format,
                width as i32,
                height as i32,
                dst_format,
                0,
                std::ptr::null_mut(),
                std::ptr::null_mut(),
                std::ptr::null_mut(),
            )
        };

        if sws_context.is_null() {
            Err(AvFormatConverterCreationError::ContextCreationError)
        } else {
            Ok(Self {
                sws_context,
                dst_format,
            })
        }
    }

    /// Copy `src` into `dst` while converting its pixel format.
    fn convert(
        &mut self,
        src: &ffi::AVFrame,
        dst: &mut OutputBuffer,
    ) -> Result<(), AvFormatConvertError> {
        let line_width = dst
            .resource
            .planes
            .get(0)
            .map(|p| p.stride as i32)
            .ok_or(AvFormatConvertError::NoPlanesInResource)?;

        // Safe because `av_image_get_buffer_size` does not take any pointer.
        let required_buffer_size =
            unsafe { ffi::av_image_get_buffer_size(self.dst_format, line_width, src.height, 1) }
                as usize;

        // Map the destination resource so the conversion function can write into it.
        let mapping = match &dst.resource.handle {
            GuestResourceHandle::Object(handle) => {
                MemoryMapping::from_fd(&handle.desc, required_buffer_size)
                    .map_err(AvFormatConvertError::CannotMapBuffer)?
            }
            GuestResourceHandle::GuestMem(handle) => {
                if handle.length < required_buffer_size {
                    return Err(AvFormatConvertError::DestinationBufferTooSmall(
                        handle.length,
                        required_buffer_size,
                    ));
                }
                MemoryMapping::from_fd_offset(&handle.desc, required_buffer_size, handle.offset)
                    .map_err(AvFormatConvertError::CannotMapBuffer)?
            }
        };

        let mut dst_stride = [0i32; 4];
        let mut dst_data = [std::ptr::null_mut(); 4];
        if dst.resource.planes.len() > 4 {
            return Err(AvFormatConvertError::TooManyPlanesInResource(
                dst.resource.planes.len(),
                4,
            ));
        }

        // An alternative would be to use av_image_fill_arrays(), but here we prefer to rely on
        // guest-provided data in case the destination buffers have some exotic layout.
        for (i, plane) in dst.resource.planes.iter().enumerate() {
            dst_stride[i] = plane.stride as i32;
            dst_data[i] = unsafe { mapping.as_ptr().add(plane.offset) };
        }

        unsafe {
            ffi::sws_scale(
                self.sws_context,
                src.data.as_ptr() as *const *const u8,
                src.linesize.as_ptr(),
                0,
                src.height,
                dst_data.as_ptr(),
                dst_stride.as_ptr(),
            );
        }

        Ok(())
    }
}

enum SessionState {
    /// Waiting for libavcodec to tell us the resolution of the stream.
    AwaitingInitialResolution,
    /// Waiting for the client to call `set_output_buffer_count`.
    AwaitingBufferCount,
    /// Decoding and producing frames.
    Decoding {
        output_queue: OutputQueue,
        format_converter: AvFormatConverter,
    },
}

pub struct FfmpegDecoderSession {
    // Queue of events waiting to be read by the client.
    event_queue: DecoderEventQueue,

    // FIFO of jobs waiting to be performed.
    codec_jobs: VecDeque<CodecJob>,
    // Whether we are currently flushing.
    is_flushing: bool,

    state: SessionState,

    // The libav context for this session.
    context: *mut ffi::AVCodecContext,
    // AVFrame used to receive decoded frames from the codec.
    avframe: *mut ffi::AVFrame,
}

impl Drop for FfmpegDecoderSession {
    fn drop(&mut self) {
        // Safe because `avframe` is valid through the life of the session.
        unsafe { ffi::av_frame_free(&mut self.avframe) };
        // Safe because `context` is valid through the life of the session.
        unsafe { ffi::avcodec_free_context(&mut self.context) };
    }
}

impl FfmpegDecoderSession {
    fn queue_event(&mut self, event: DecoderEvent) -> Result<(), AvError> {
        self.event_queue
            .queue_event(event)
            .map_err(AvError::BrokenEventPipe)
    }

    fn change_resolution(&mut self) -> Result<(), AvError> {
        // Safe because `context` is valid through the life of the session.
        let avcontext = unsafe { &*self.context };
        let new_visible_res = (avcontext.width, avcontext.height);
        let new_coded_res = (avcontext.coded_width, avcontext.coded_height);
        info!("resolution changed to {:?}", (new_coded_res));

        // Ask the client for new buffers.
        self.queue_event(DecoderEvent::ProvidePictureBuffers {
            min_num_buffers: std::cmp::max(avcontext.refs, 0) as u32 + 1,
            width: new_coded_res.0,
            height: new_coded_res.1,
            visible_rect: Rect {
                left: 0,
                top: 0,
                right: new_visible_res.0,
                bottom: new_visible_res.1,
            },
        })?;

        // Drop our output queue and wait for the new number of output buffers.
        self.state = SessionState::AwaitingBufferCount;

        Ok(())
    }

    /// Try to send one of the pending input packets to the codec.
    ///
    /// Returns `true` if a packet has successfully been queued, `false` if it could not, either
    /// because all pending work has already been queued or because the codec could not accept any
    /// extra input.
    fn send_packet(&mut self, input_packet: &InputPacket) -> Result<bool, AvError> {
        // Safe because `context` is valid through the life of the session.
        let avcontext = unsafe { &*self.context };
        let current_visible_res = (avcontext.width, avcontext.height);
        let current_coded_res = (avcontext.coded_width, avcontext.coded_height);

        let InputPacket {
            bitstream_id,
            resource,
            offset,
            bytes_used,
        } = input_packet;

        let mapping = match resource {
            GuestResourceHandle::Object(handle) => {
                MemoryMapping::from_fd_offset(&handle.desc, *bytes_used as usize, *offset as u64)
            }
            GuestResourceHandle::GuestMem(handle) => MemoryMapping::from_fd_offset(
                &handle.desc,
                *bytes_used as usize,
                handle.offset + (*offset as u64),
            ),
        }
        .unwrap();
        // Safe because `mapping` has been successfully created, hence that range of memory is valid.
        let input_data =
            unsafe { std::slice::from_raw_parts_mut(mapping.as_ptr(), mapping.size()) };

        // Prepare our AVPacket and ask the codec to process it.
        let avpacket = ffi::AVPacket {
            buf: std::ptr::null_mut(),
            pts: *bitstream_id as i64,
            dts: ffi::AV_NOPTS_VALUE,
            data: input_data.as_mut_ptr(),
            size: input_data.len() as i32,
            side_data: std::ptr::null_mut(),
            pos: -1,
            // Safe because all the other elements of this struct can be zeroed.
            ..unsafe { std::mem::zeroed() }
        };

        // Safe because `context` is valid through the life of the session, and the data fields of
        // `avpacket` point to valid memory.
        match unsafe { ffi::avcodec_send_packet(self.context, &avpacket) } {
            0 => {}
            // If we got invalid data, keep going in hope that we will catch a valid state later.
            ffi::AVERROR_INVALIDDATA => warn!("Invalid data in stream, ignoring..."),
            // The codec cannot take any more buffer, we'll try again after we receive some frames.
            AVERROR_EAGAIN => return Ok(false),
            err => return Err(AvError::SendPacketError(err)),
        }

        // The input buffer has been processed and can be reused.
        drop(mapping);
        self.queue_event(DecoderEvent::NotifyEndOfBitstreamBuffer(*bitstream_id))?;

        // Re-cast again as `self.context` has changed, to prevent caching optimizations from
        // returning old values.
        // Safe because `context` is valid through the life of the session.
        let avcontext = unsafe { &*self.context };

        // Now check whether the resolution of the stream has changed.
        let new_visible_res = (avcontext.width, avcontext.height);
        let new_coded_res = (avcontext.coded_width, avcontext.coded_height);
        if new_visible_res != current_visible_res || new_coded_res != current_coded_res {
            self.change_resolution()?;
        }

        Ok(true)
    }

    fn try_run_job(&mut self) -> Result<bool, AvError> {
        let next_job = match self.codec_jobs.pop_front() {
            // No work to do at the moment.
            None => return Ok(false),
            Some(job) => job,
        };

        match &next_job {
            CodecJob::Packet(input_packet) => {
                let res = self.send_packet(input_packet);
                // Put the packet's job back into the queue if the codec could not accept it at
                // the moment.
                if let Ok(false) = res {
                    self.codec_jobs.push_front(next_job);
                }

                res
            }
            CodecJob::Flush => {
                // Send a NULL packet to signal the codec we want to flush. It will make
                // `av_receive_frame` return `AVERROR_EOF` when the flush is completed.
                // Safe because `context` is valid through the life of the session.
                match unsafe { ffi::avcodec_send_packet(self.context, std::ptr::null()) } {
                    0 => self.is_flushing = true,
                    err => self.queue_event(DecoderEvent::FlushCompleted(Err(
                        AvError::SendPacketError(err).into(),
                    )))?,
                }

                Ok(true)
            }
        }
    }

    /// Try to receive a frame from the codec and emit the corresponding `PictureReady` decoder
    /// event.
    ///
    /// Returns `true` if a frame was successfully retrieved, or `None` if no frame was available at
    /// the time or the decoder needs more input data to proceed further.
    fn try_receive_frame(&mut self) -> Result<bool, AvError> {
        let (output_queue, format_converter) = match &mut self.state {
            SessionState::Decoding {
                output_queue,
                format_converter,
            } => (output_queue, format_converter),
            // We are not in a state where we can receive frames.
            _ => return Ok(false),
        };

        let target_buffer = match output_queue.try_get_ready_buffer() {
            None => return Ok(false),
            Some(buffer) => buffer,
        };
        let picture_buffer_id = target_buffer.picture_buffer_id;

        // Safe because `context` and `avframe` are valid through the life of the session.
        match unsafe { ffi::avcodec_receive_frame(self.context, self.avframe) } {
            0 => {}
            // The codec is not ready, put the buffer back into the queue and exit.
            AVERROR_EAGAIN => {
                output_queue.reuse_buffer(picture_buffer_id)?;
                return Ok(false);
            }
            // Signals that a flush request has completed.
            ffi::AVERROR_EOF => {
                self.is_flushing = false;
                self.queue_event(DecoderEvent::FlushCompleted(Ok(())))?;
                // Safe because `context` is valid through the life of the session.
                unsafe {
                    ffi::avcodec_flush_buffers(self.context);
                }
                return Ok(true);
            }
            err => {
                // This is a decoding error, so signal it using a `NotifyError` event to reflect the
                // same asynchronous flow as a hardware decoder.
                self.event_queue
                    .queue_event(DecoderEvent::NotifyError(
                        AvError::ReceiveFrameError(err).into(),
                    ))
                    .map_err(AvError::BrokenEventPipe)?;
                return Ok(false);
            }
        }

        // Safe because `self.avframe` is valid through the life of the session.
        let avframe = unsafe { &*self.avframe };
        format_converter.convert(avframe, target_buffer)?;

        self.event_queue
            .queue_event(DecoderEvent::PictureReady {
                picture_buffer_id: target_buffer.picture_buffer_id,
                bitstream_id: avframe.pts as i32,
                visible_rect: Rect {
                    left: 0,
                    top: 0,
                    right: avframe.width,
                    bottom: avframe.height,
                },
            })
            .map_err(AvError::BrokenEventPipe)?;

        Ok(true)
    }

    fn try_decode(&mut self) -> Result<(), AvError> {
        loop {
            // Do not send any new job to the codec while a flush is in progress.
            let job_run = if !self.is_flushing {
                self.try_run_job()?
            } else {
                false
            };
            let frame_received = self.try_receive_frame()?;

            // We cannot progress until we get extra input data or output buffers.
            if !job_run && !frame_received {
                return Ok(());
            }
        }
    }
}

impl DecoderSession for FfmpegDecoderSession {
    fn set_output_parameters(&mut self, buffer_count: usize, format: Format) -> VideoResult<()> {
        match self.state {
            SessionState::AwaitingBufferCount => {
                // Safe because `context` is valid through the life of the session.
                let avcontext = unsafe { &*self.context };

                self.state = SessionState::Decoding {
                    output_queue: OutputQueue::new(buffer_count),
                    format_converter: AvFormatConverter::new(
                        avcontext.width as usize,
                        avcontext.height as usize,
                        avcontext.pix_fmt,
                        format,
                    )
                    .map_err(AvError::FormatConverterCreationError)?,
                };
                Ok(())
            }
            _ => Err(AvError::InvalidState.into()),
        }
    }

    fn decode(
        &mut self,
        bitstream_id: i32,
        resource: GuestResourceHandle,
        offset: u32,
        bytes_used: u32,
    ) -> VideoResult<()> {
        self.codec_jobs.push_back(CodecJob::Packet(InputPacket {
            bitstream_id,
            resource,
            offset,
            bytes_used,
        }));

        self.try_decode()?;

        Ok(())
    }

    fn flush(&mut self) -> VideoResult<()> {
        if self.is_flushing {
            Err(AvError::FlushInProgress.into())
        } else {
            self.codec_jobs.push_back(CodecJob::Flush);
            self.try_decode().map_err(Into::into)
        }
    }

    fn reset(&mut self) -> VideoResult<()> {
        // Reset the codec.
        // Safe because `context` is valid through the life of the session.
        unsafe {
            ffi::avcodec_flush_buffers(self.context);
        }

        // Complete ongoing flush if any.
        if self.is_flushing {
            self.is_flushing = false;
            self.queue_event(DecoderEvent::FlushCompleted(Ok(())))?;
        }

        // Complete any pending job.
        let mut old_jobs = std::mem::take(&mut self.codec_jobs);
        for job in old_jobs.drain(..) {
            match job {
                CodecJob::Packet(InputPacket { bitstream_id, .. }) => {
                    self.queue_event(DecoderEvent::NotifyEndOfBitstreamBuffer(bitstream_id))?
                }
                CodecJob::Flush => self.queue_event(DecoderEvent::FlushCompleted(Ok(())))?,
            }
        }

        // TODO should we also drop the currently queued output buffers?

        // TODO Does this event need to exist at all, when the reset is supposed to be completed by
        // the time this method returns?
        self.queue_event(DecoderEvent::ResetCompleted(Ok(())))?;

        Ok(())
    }

    fn event_pipe(&self) -> &dyn AsRawDescriptor {
        self.event_queue.event_pipe()
    }

    fn use_output_buffer(
        &mut self,
        picture_buffer_id: i32,
        resource: GuestResource,
    ) -> VideoResult<()> {
        let output_queue = match &mut self.state {
            SessionState::Decoding { output_queue, .. } => output_queue,
            _ => return Err(AvError::InvalidState.into()),
        };
        let buffer = OutputBuffer {
            picture_buffer_id,
            resource,
        };

        output_queue
            .import_buffer(buffer)
            .map_err(AvError::CannotImportBuffer)?;
        self.try_decode()?;

        Ok(())
    }

    fn reuse_output_buffer(&mut self, picture_buffer_id: i32) -> VideoResult<()> {
        let output_queue = match &mut self.state {
            SessionState::Decoding { output_queue, .. } => output_queue,
            _ => return Err(AvError::InvalidState.into()),
        };

        output_queue
            .reuse_buffer(picture_buffer_id)
            .map_err(AvError::CannotReuseBuffer)?;
        self.try_decode()?;

        Ok(())
    }

    fn read_event(&mut self) -> VideoResult<DecoderEvent> {
        self.event_queue
            .dequeue_event()
            .map_err(|e| AvError::BrokenEventPipe(e).into())
    }
}

pub struct FfmpegDecoder {
    codecs: BTreeMap<Format, &'static ffi::AVCodec>,
}

impl FfmpegDecoder {
    fn new() -> Self {
        let mut supported_codecs: BTreeMap<Format, &'static ffi::AVCodec> = BTreeMap::new();

        // Find all the decoders supported by libav and store them.
        for codec in AvCodecIterator::new() {
            // Safe because `av_codec_is_decoder` is called on a valid static `AVCodec` instance.
            if unsafe { ffi::av_codec_is_decoder(codec) } == 0 {
                continue;
            }

            // Safe because `CStr::from_ptr` is called on a valid zero-terminated C string.
            let name = match unsafe { CStr::from_ptr(codec.name).to_str() } {
                Ok(name) => name,
                Err(_) => continue,
            };

            // Only keep processing the decoders we are interested in. These are all software
            // decoders, but nothing prevents us from supporting hardware-accelerated ones
            // (e.g. *_qsv for VAAPI-based acceleration) in the future!
            let format = match name {
                "h264" => Format::H264,
                "vp8" => Format::VP8,
                "vp9" => Format::VP9,
                "hevc" => Format::HEVC,
                _ => continue,
            };

            if codec.capabilities as u32 & ffi::AV_CODEC_CAP_DR1 == 0 {
                warn!("Skipping codec {} due to lack of DR1 capability.", name);
                continue;
            }

            supported_codecs.insert(format, codec);
        }

        Self {
            codecs: supported_codecs,
        }
    }
}

impl DecoderBackend for FfmpegDecoder {
    type Session = FfmpegDecoderSession;

    fn get_capabilities(&self) -> Capability {
        // The virtio device only supports NV12 for now it seems...
        const SUPPORTED_OUTPUT_FORMATS: [Format; 1] = [Format::NV12];

        let mut in_formats = vec![];
        let mut profiles_map: BTreeMap<Format, Vec<Profile>> = Default::default();
        let mut levels: BTreeMap<Format, Vec<Level>> = Default::default();
        for (&format, &codec) in &self.codecs {
            let profile_iter = AvProfileIterator::new(codec);
            let profiles = match format {
                Format::H264 => {
                    // We only support Level 1.0 for H.264.
                    // TODO Do we? Why?
                    levels.insert(format, vec![Level::H264_1_0]);

                    profile_iter
                        .filter_map(|p| {
                            match p.profile {
                                ffi::FF_PROFILE_H264_BASELINE => Some(Profile::H264Baseline),
                                ffi::FF_PROFILE_H264_MAIN => Some(Profile::H264Main),
                                ffi::FF_PROFILE_H264_EXTENDED => Some(Profile::H264Extended),
                                ffi::FF_PROFILE_H264_HIGH => Some(Profile::H264High),
                                ffi::FF_PROFILE_H264_HIGH_10 => Some(Profile::H264High10),
                                ffi::FF_PROFILE_H264_HIGH_422 => Some(Profile::H264High422),
                                ffi::FF_PROFILE_H264_HIGH_444_PREDICTIVE => {
                                    Some(Profile::H264High444PredictiveProfile)
                                }
                                ffi::FF_PROFILE_H264_STEREO_HIGH => Some(Profile::H264StereoHigh),
                                ffi::FF_PROFILE_H264_MULTIVIEW_HIGH => {
                                    Some(Profile::H264MultiviewHigh)
                                }
                                // TODO H264ScalableBaseline and H264ScalableHigh have no libav
                                // equivalents?
                                _ => None,
                            }
                        })
                        .collect()
                }
                Format::VP8 => {
                    // FFmpeg has no VP8 profiles, for some reason...
                    vec![
                        Profile::VP8Profile0,
                        Profile::VP8Profile1,
                        Profile::VP8Profile2,
                        Profile::VP8Profile3,
                    ]
                }
                Format::VP9 => profile_iter
                    .filter_map(|p| match p.profile {
                        ffi::FF_PROFILE_VP9_0 => Some(Profile::VP9Profile0),
                        ffi::FF_PROFILE_VP9_1 => Some(Profile::VP9Profile1),
                        ffi::FF_PROFILE_VP9_2 => Some(Profile::VP9Profile2),
                        ffi::FF_PROFILE_VP9_3 => Some(Profile::VP9Profile3),
                        _ => None,
                    })
                    .collect(),
                Format::HEVC => profile_iter
                    .filter_map(|p| match p.profile {
                        ffi::FF_PROFILE_HEVC_MAIN => Some(Profile::HevcMain),
                        ffi::FF_PROFILE_HEVC_MAIN_10 => Some(Profile::HevcMain10),
                        ffi::FF_PROFILE_HEVC_MAIN_STILL_PICTURE => {
                            Some(Profile::HevcMainStillPicture)
                        }
                        _ => None,
                    })
                    .collect(),
                _ => unreachable!("Unhandled format {:?}", format),
            };

            profiles_map.insert(format, profiles);

            in_formats.push(FormatDesc {
                mask: !(u64::MAX << SUPPORTED_OUTPUT_FORMATS.len()),
                format,
                frame_formats: vec![Default::default()],
            });
        }

        // We support all output formats through the use of swscale().
        let out_formats = SUPPORTED_OUTPUT_FORMATS
            .iter()
            .map(|&format| FormatDesc {
                mask: !(u64::MAX << in_formats.len()),
                format,
                frame_formats: vec![FrameFormat {
                    // TODO do not use hardcoded values.
                    width: FormatRange {
                        min: 64,
                        max: 4096,
                        step: 1,
                    },
                    height: FormatRange {
                        min: 64,
                        max: 4096,
                        step: 1,
                    },
                    bitrates: Default::default(),
                }],
            })
            .collect::<Vec<_>>();

        Capability::new(in_formats, out_formats, profiles_map, levels)
    }

    fn new_session(&mut self, format: Format) -> VideoResult<Self::Session> {
        let codec = *self.codecs.get(&format).ok_or(VideoError::InvalidFormat)?;

        // Safe because `codec` is a valid static AVCodec reference.
        let mut context = unsafe { ffi::avcodec_alloc_context3(codec).as_mut() }
            .ok_or(AvError::ContextAllocation)?;

        context.opaque = self as *const FfmpegDecoder as *mut libc::c_void;
        context.get_buffer2 = Some(get_buffer2);
        context.thread_safe_callbacks = 1;

        // Safe because both `codec` is a valid static AVCodec reference, and `context` has been
        // successfully allocated above.
        if unsafe { ffi::avcodec_open2(context, codec, std::ptr::null_mut()) } < 0 {
            return Err(AvError::ContextOpen.into());
        }

        // Safe because `av_frame_alloc` does not take any input.
        let avframe = unsafe { ffi::av_frame_alloc().as_mut() }.ok_or(AvError::FrameAllocate)?;

        Ok(FfmpegDecoderSession {
            codec_jobs: Default::default(),
            is_flushing: false,
            state: SessionState::AwaitingInitialResolution,
            event_queue: DecoderEventQueue::new().map_err(AvError::BrokenEventPipe)?,
            context,
            avframe,
        })
    }
}

/// # Warning
/// This function must be thread-safe as per libavcodec's documentation.
unsafe extern "C" fn get_buffer2(
    context: *mut ffi::AVCodecContext,
    frame: *mut ffi::AVFrame,
    flags: i32,
) -> i32 {
    // TODO try to directly use the output buffers if the pixel format matches?
    ffi::avcodec_default_get_buffer2(context, frame, flags)
}

pub fn new() -> FfmpegDecoder {
    FfmpegDecoder::new()
}

#[cfg(test)]
mod tests {
    use crate::virtio::video::{format::FramePlane, resource::GuestObjectHandle};

    use super::*;
    use base::{FromRawDescriptor, SafeDescriptor};
    use std::io::Write;
    use sys_util::{MemoryMapping, SharedMemory};

    const H264_STREAM: &[u8] = include_bytes!("test-25fps.h264");

    /// Splits a H.264 annex B stream into chunks that are all guaranteed to contain a full frame
    /// worth of data.
    ///
    /// This is a pretty naive implementation that is only guaranteed to work with our examples.
    /// We are not using `AVCodecParser` because it seems to modify the decoding context, which
    /// would result in testing conditions that diverge more from our real use case where parsing
    /// has already been done.
    struct H264NalIterator<'a> {
        stream: &'a [u8],
        pos: usize,
    }

    impl<'a> H264NalIterator<'a> {
        fn new(stream: &'a [u8]) -> Self {
            Self { stream, pos: 0 }
        }

        /// Returns the position of the start of the next frame in the stream.
        fn next_frame_pos(&self) -> Option<usize> {
            const H264_START_CODE: [u8; 4] = [0x0, 0x0, 0x0, 0x1];
            self.stream[self.pos + 1..]
                .windows(H264_START_CODE.len())
                .position(|window| window == H264_START_CODE)
                .map(|pos| self.pos + pos + 1)
        }

        /// Returns whether `slice` contains frame data, i.e. a header where the NAL unit type is
        /// 0x1 or 0x5.
        fn contains_frame(slice: &[u8]) -> bool {
            slice[4..].windows(4).any(|window| {
                window[0..3] == [0x0, 0x0, 0x1]
                    && (window[3] & 0x1f == 0x5 || window[3] & 0x1f == 0x1)
            })
        }
    }

    impl<'a> Iterator for H264NalIterator<'a> {
        type Item = &'a [u8];

        fn next(&mut self) -> Option<Self::Item> {
            match self.pos {
                cur_pos if cur_pos == self.stream.len() => None,
                cur_pos => loop {
                    self.pos = self.next_frame_pos().unwrap_or(self.stream.len());
                    let slice = &self.stream[cur_pos..self.pos];

                    // Keep advancing as long as we don't have frame data in our slice.
                    if Self::contains_frame(slice) || self.pos == self.stream.len() {
                        return Some(slice);
                    }
                },
            }
        }
    }

    #[test]
    fn get_capabilities() {
        let decoder = FfmpegDecoder::new();
        decoder.get_capabilities();
    }

    #[test]
    // Full decoding test of a H.264 video, checking that the flow of events is happening as
    // expected. Input and output buffers are both backed by shared memory mimicking a virtio
    // object.
    fn decode_dmabuf() {
        // TODO remove this
        let mut output_file = std::fs::File::create("decoded_frames.yuv").unwrap();

        const NUM_OUTPUT_BUFFERS: usize = 4;
        const INPUT_BUF_SIZE: usize = 0x4000;
        const VIDEO_WIDTH: i32 = 320;
        const VIDEO_HEIGHT: i32 = 240;
        const VIDEO_NUM_FRAMES: usize = 250;
        const OUTPUT_BUFFER_SIZE: usize =
            (VIDEO_WIDTH * (VIDEO_HEIGHT + VIDEO_HEIGHT / 2)) as usize;
        let mut decoder = FfmpegDecoder::new();
        let mut session = decoder
            .new_session(Format::H264)
            .expect("failed to create H264 decoding session.");
        // Output buffers suitable for receiving NV12 frames for our stream.
        let output_buffers = (0..NUM_OUTPUT_BUFFERS)
            .into_iter()
            .map(|_| {
                let mut shm = SharedMemory::anon().unwrap();
                shm.set_size(OUTPUT_BUFFER_SIZE as u64).unwrap();
                shm
            })
            .collect::<Vec<_>>();
        let mut input_shm = SharedMemory::anon().unwrap();
        input_shm.set_size(INPUT_BUF_SIZE as u64).unwrap();
        let mapping = MemoryMapping::from_fd(&input_shm, input_shm.size() as usize).unwrap();

        let mut decoded_frames_count = 0usize;

        let mut on_frame_decoded =
            |session: &mut FfmpegDecoderSession, picture_buffer_id: i32, visible_rect: Rect| {
                assert_eq!(
                    visible_rect,
                    Rect {
                        left: 0,
                        top: 0,
                        right: VIDEO_WIDTH,
                        bottom: VIDEO_HEIGHT,
                    }
                );

                // Write frame contents to the output file.
                let mapping = MemoryMapping::from_fd(
                    &output_buffers[picture_buffer_id as usize],
                    OUTPUT_BUFFER_SIZE,
                )
                .unwrap();
                let slice = unsafe { std::slice::from_raw_parts(mapping.as_ptr(), mapping.size()) };
                output_file.write_all(slice).unwrap();

                // We can recycle the frame.
                session.reuse_output_buffer(picture_buffer_id).unwrap();
                decoded_frames_count += 1;
            };

        for (i, slice) in H264NalIterator::new(H264_STREAM).enumerate() {
            let buffer_handle = GuestResourceHandle::Object(GuestObjectHandle {
                // Safe because we are building a File from a freshly cloned FD and are the
                // exclusive owner.
                desc: unsafe {
                    SafeDescriptor::from_raw_descriptor(base::clone_fd(&input_shm).unwrap())
                },
                modifier: 0,
            });
            mapping
                .write_slice(slice, 0)
                .expect("Failed to write stream data into input buffer.");
            // TODO looks like this is not closing the FD as it promises it would!
            session
                .decode(i as i32, buffer_handle, 0, slice.len() as u32)
                .expect("Call to decode() failed.");

            assert!(
                matches!(session.read_event().unwrap(), DecoderEvent::NotifyEndOfBitstreamBuffer(index) if index == i as i32)
            );

            // After sending the first buffer we should get the initial resolution change event and
            // can provide the frames to decode into.
            if i == 0 {
                assert!(matches!(
                    session.read_event().unwrap(),
                    DecoderEvent::ProvidePictureBuffers {
                        min_num_buffers: 3,
                        width: VIDEO_WIDTH,
                        height: VIDEO_HEIGHT,
                        visible_rect: Rect {
                            left: 0,
                            top: 0,
                            right: VIDEO_WIDTH,
                            bottom: VIDEO_HEIGHT,
                        },
                    }
                ));

                session
                    .set_output_parameters(NUM_OUTPUT_BUFFERS, Format::NV12)
                    .unwrap();

                // Pass the buffers we will decode into.
                for (i, buffer) in output_buffers.iter().enumerate() {
                    session
                        .use_output_buffer(
                            i as i32,
                            GuestResource {
                                handle: GuestResourceHandle::Object(GuestObjectHandle {
                                    // Safe because we are taking ownership of a just-duplicated FD.
                                    desc: unsafe {
                                        SafeDescriptor::from_raw_descriptor(
                                            base::clone_fd(&buffer).unwrap(),
                                        )
                                    },
                                    modifier: 0,
                                }),

                                planes: vec![
                                    FramePlane {
                                        offset: 0,
                                        stride: VIDEO_WIDTH as usize,
                                    },
                                    FramePlane {
                                        offset: (VIDEO_WIDTH * VIDEO_HEIGHT) as usize,
                                        stride: VIDEO_WIDTH as usize,
                                    },
                                ],
                            },
                        )
                        .unwrap();
                }
            }

            // If we have remaining events, they must be decoded frames. Get them and recycle them.
            while session.event_queue.len() > 0 {
                match session.read_event().unwrap() {
                    DecoderEvent::PictureReady {
                        picture_buffer_id,
                        visible_rect,
                        ..
                    } => on_frame_decoded(&mut session, picture_buffer_id, visible_rect),
                    e => panic!("Unexpected event: {:?}", e),
                }
            }

            // We should have read all the pending events for that frame.
            assert_eq!(session.event_queue.len(), 0);
        }

        session.flush().unwrap();

        // Keep getting frames until the final event, which should be `FlushCompleted`.
        while session.event_queue.len() > 1 {
            match session.read_event().unwrap() {
                DecoderEvent::PictureReady {
                    picture_buffer_id,
                    visible_rect,
                    ..
                } => on_frame_decoded(&mut session, picture_buffer_id, visible_rect),
                e => panic!("Unexpected event: {:?}", e),
            }
        }

        // Get the FlushCompleted event.
        assert!(matches!(
            session.read_event().unwrap(),
            DecoderEvent::FlushCompleted(Ok(()))
        ));

        // We should have read all the events for that session.
        assert_eq!(session.event_queue.len(), 0);

        // Check that we decoded the expected number of frames.
        assert_eq!(decoded_frames_count, VIDEO_NUM_FRAMES);
    }
}
